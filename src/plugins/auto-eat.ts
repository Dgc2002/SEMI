/// <reference path='../../types/melvor.d.ts'/>

(() => {
    const id = 'auto-eat';
    const title = 'AutoEat';
    const desc =
        "AutoEat in combat will still only eat when your HP is below calculated max hit, but now it will eat to nearly full health instead of just to the max hit value. It eats with efficiency in mind, so it won't eat food if your food healing would cause overhealing. However, it will still eat if you are at risk of dying from max hit near full health, ignoring this efficiency rule. This makes it a little more viable for the v0.16+ combat eating changes. Suggestion: large quantities of low-healing food for maximum efficiency of this bot. Also, if you don't have dungeon equipment swapping and you run out of food without AutoRun also enabled in a dungeon, you will just die.";
    const imgSrc = 'assets/media/shop/autoeat.svg';

    //@ts-expect-error
    const isInCombat = () => player.manager.isInCombat;

    const autoEat = () => {
        const hp = SEMIUtils.currentHP(); // this number is already multiplied
        const hpmax = SEMIUtils.maxHP();
        const hpdeficit = hpmax - hp;
        //@ts-expect-error
        const currentFood = player.food.slots[player.food.selectedSlot];
        const hpfood = numberMultiplier * currentFood.item.healsFor;
        const adjustedMaxHit = SEMIUtils.adjustedMaxHit();
        const maxHitEatingCase = hp <= adjustedMaxHit && isInCombat();
        const thievingMaxHit = Math.max(...thievingNPC.map((npc) => getNumberMultiplierValue(npc.maxHit)));
        const generalEatingCase =
            (hpdeficit > hpfood || hp <= thievingMaxHit) && !SEMIUtils.isCurrentSkill('Hitpoints');
        const haveFoodEquipped = currentFood.quantity > 0;
        const eatingCase = (maxHitEatingCase || generalEatingCase) && haveFoodEquipped;

        const logItAll = () => {
            console.log(
                'hp',
                hp,
                'hpmax',
                hpmax,
                'hpdeficit',
                hpdeficit,
                'currentFood',
                currentFood,
                'hpfood',
                hpfood,
                'adjustedMaxHit',
                adjustedMaxHit,
                'maxHitEatingCase',
                maxHitEatingCase,
                'thievingMaxHit',
                thievingMaxHit,
                'generalEatingCase',
                generalEatingCase,
                'eatingCase',
                eatingCase
            );
        };

        if (eatingCase) {
            //@ts-expect-error
            player.eatFood();
            if (!SEMIUtils.isCurrentSkill('Hitpoints')) return;

            // logItAll();
            //prettier-ignore
            while (
                SEMIUtils.currentHP() <= hpmax - hpfood //while your current health is less than max by an amount more than what your food would heal, so as to not be wasteful (ex eat 300hp heal at 10hp deficit)
                && hpmax > adjustedMaxHit //and your max hitpoints are greater than the adjusted max hit of your current enemy, otherwise, you're boned
                && currentFood.quantity >= 1 //and you actually have food equipped to eat
            ) { //run a loop to eat multiple times instantaneously until your HP reaches the point where eating any more would be wasteful
                //@ts-expect-error
                player.eatFood();
            }
        }
        if (currentFood.quantity >= 1) {
            return;
        }

        //@ts-expect-error
        const isInDungeon = player.manager.areaData.type === 'Dungeon';
        //@ts-expect-error
        const characterHasDungeonEquipmentSwap = player.modifiers.dungeonEquipmentSwapping === 1;
        if (currentFood.quantity === 0 && isInDungeon && !characterHasDungeonEquipmentSwap) {
            return;
        }
        //@ts-expect-error
        for (let i = 0; i < player.food.slots.length; i++) {
            //@ts-expect-error
            if (player.food.slots[i].quantity > 0) {
                //@ts-expect-error
                return player.selectFood(i);
            }
        }
    };

    const onEnable = () => {
        const hpmax = SEMIUtils.maxHP();
        const adjustedMaxHit = SEMIUtils.adjustedMaxHit();
        if (hpmax <= adjustedMaxHit) {
            SEMIUtils.customNotify(
                'assets/media/monsters/ghost.svg',
                "WARNING: You are engaged with an enemy that can one-hit-kill you. \n Its damage-reduction-adjusted max hit is at or above your max HP. \n This script can't save you now.",
                { duration: 10000 }
            );
        }
    };

    SEMI.add(id, {
        ms: 100,
        onLoop: autoEat,
        onEnable,
        pluginType: SEMI.PLUGIN_TYPE.AUTO_COMBAT,
        title,
        imgSrc,
        desc,
    });
})();
